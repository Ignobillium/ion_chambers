/// \file KhankinDetectorMessenger.cc
/// \brief Implementation of the Khankin::DetectorMessenger class

#include "DetectorMessenger.hh"
#include "DetectorConstruction.hh"

#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithAnInteger.hh"

namespace B1
{

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorMessenger::DetectorMessenger(DetectorConstruction* det)
 : fDetectorConstruction(det)
{
  fDirectory = new G4UIdirectory("/");
  fDirectory->SetGuidance("UI commands specific to this example.");

  fDetDirectory = new G4UIdirectory("/det/");
  fDetDirectory->SetGuidance("Detector construction control");

  fDetSetDirectory = new G4UIdirectory("/det/set/");
  fDetSetDirectory->SetGuidance("Set new value to parameter");

  fDetGetDirectory = new G4UIdirectory("/det/get/");
  fDetGetDirectory->SetGuidance("Get current value of the parameter");
  // TODO directory /det/get

  // === * === * === * === * === * === * === * === * === * === * === * === * ===
  // Set commands section
  //
  fSetPlateMaterial = new G4UIcmdWithAString("/det/set/plateMaterial",this);
  fSetPlateMaterial->SetGuidance("Select Material of the Plate.");
  fSetPlateMaterial->SetParameterName("plate_material", false);
  fSetPlateMaterial->AvailableForStates(G4State_PreInit);

  fSetPlateThickness = new G4UIcmdWithADoubleAndUnit("/det/set/plateThickness",this);
  fSetPlateThickness->SetGuidance("Set modifier-plate thickness (z-dimension full length)");
  fSetPlateThickness->SetParameterName("plate_thickness", false);
  fSetPlateThickness->SetUnitCategory("Length");
  fSetPlateThickness->AvailableForStates(G4State_PreInit);

  // === * === * === * === * === * === * === * === * === * === * === * === * ===
  // Get commands section
  //
  // TODO
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorMessenger::~DetectorMessenger()
{
  
  delete fSetPlateMaterial;
  delete fSetPlateThickness;

  delete fDirectory;
  delete fDetDirectory;
  delete fDetSetDirectory;
  delete fDetGetDirectory;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DetectorMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{
  if (command == fSetPlateMaterial) { 
    fDetectorConstruction->SetAttenuatorMaterial(
      newValue
    );
  } else if (command == fSetPlateThickness) { 
    fDetectorConstruction->SetAttenuatorThickness(
      fSetPlateThickness->GetNewDoubleValue(newValue)
    );
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

}
