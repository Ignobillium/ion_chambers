//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file DetectorConstruction.cc
/// \brief Implementation of the B1::DetectorConstruction class

#include "DetectorConstruction.hh"

#include "G4RunManager.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Cons.hh"
#include "G4Orb.hh"
#include "G4Sphere.hh"
#include "G4Trd.hh"

#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4SystemOfUnits.hh"
#include "G4RegionStore.hh"
#include "G4ProductionCuts.hh"
#include "G4Region.hh"
#include "G4UserLimits.hh"

#include "DetectorMessenger.hh"

namespace B1
{

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::DetectorConstruction()
{
  this->fMess = new DetectorMessenger(this);

  this->thickness = 0.97 * cm;
  this->material = G4NistManager::Instance()->FindOrBuildMaterial("G4_Al");
  this->attenuator_tlate = new G4ThreeVector(0, 0, this->thickness / 2.);

  G4NistManager* nist = G4NistManager::Instance();
  auto PMMA = nist->FindOrBuildMaterial("G4_PLEXIGLASS"); // default 1.19 g/cm3
  auto TiO2 = nist->FindOrBuildMaterial("G4_TITANIUM_DIOXIDE");
  auto Polystyrene = nist->FindOrBuildMaterial("G4_POLYSTYRENE");
  auto air = nist->FindOrBuildMaterial("G4_AIR");
  G4Material* SolidWater = new G4Material("SolidWater", 1.045 * g / cm3, 2);
  SolidWater->AddMaterial(TiO2, 2.1 * perCent);
  SolidWater->AddMaterial(Polystyrene, (100-2.1) * perCent);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::~DetectorConstruction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DetectorConstruction::SetAttenuatorMaterial(G4String& new_material_name) {
  // TODO логировать!
  // Рассчитываем новые значения.
  auto fNewMaterial = G4NistManager::Instance()->FindOrBuildMaterial(new_material_name);
  
  if(fNewMaterial == nullptr){
    // G4cerr << "Invalid material " << new_material_name << ". fPlateMAterial stays the same one." << G4endl;
    G4cerr << "Invalid material "  << new_material_name << G4endl;
    std::exit(1);
    // TODO throw exception
    return;
  }

  this->material = fNewMaterial;
}

void DetectorConstruction::SetAttenuatorThickness(G4double new_thickness) {
  this->thickness = new_thickness;
  this->attenuator_tlate->setZ(this->thickness / 2.0);
}

G4VPhysicalVolume* DetectorConstruction::Construct()
{
  G4Region* IonChamberRegion = new G4Region("IonChamberRegion");
  G4Region* AttenuatorRegion = new G4Region("AttenuatorRegion");

  // Get nist material manager
  G4NistManager* nist = G4NistManager::Instance();

  auto Cu = nist->FindOrBuildMaterial("G4_Cu");
  auto PMMA = nist->FindOrBuildMaterial("G4_PLEXIGLASS"); // default 1.19 g/cm3
  auto TiO2 = nist->FindOrBuildMaterial("G4_TITANIUM_DIOXIDE");
  auto Polystyrene = nist->FindOrBuildMaterial("G4_POLYSTYRENE");
  auto air = nist->FindOrBuildMaterial("G4_AIR");


  G4Material* SolidWater = nist->FindOrBuildMaterial("SolidWater");

  // Envelope parameters
  //
  G4double env_sizeXY = 40*cm, env_sizeZ = 30*cm;

  // Option to switch on/off checking of volumes overlaps
  //
  G4bool checkOverlaps = true;

  //
  // World
  //
  G4double world_sizeXY = 1.2*env_sizeXY;
  G4double world_sizeZ  = 400*cm;
  G4Material* world_mat = nist->FindOrBuildMaterial("G4_AIR");

  G4Box* solidWorld =
    new G4Box("World",                       //its name
       0.5*world_sizeXY, 0.5*world_sizeXY, 0.5*world_sizeZ);     //its size

  G4LogicalVolume* logicWorld =
    new G4LogicalVolume(solidWorld,          //its solid
                        world_mat,           //its material
                        "World");            //its name

  G4VPhysicalVolume* physWorld =
    new G4PVPlacement(0,                     //no rotation
                      G4ThreeVector(),       //at (0,0,0)
                      logicWorld,            //its logical volume
                      "World",               //its name
                      0,                     //its mother  volume
                      false,                 //no boolean operation
                      0,                     //copy number
                      checkOverlaps);        //overlaps checking

  auto water = nist->FindOrBuildMaterial("G4_WATER");

  // logicWorld->SetRegion(WorldRegion);
  // WorldRegion->AddRootLogicalVolume(logicWorld);

  // construct basement of solid water
  G4Box* basement_svol = new G4Box("Basement", 15. * cm, 15. * cm, 1 * cm);
  G4LogicalVolume* basement_lvol = new G4LogicalVolume(basement_svol, SolidWater, "Basement");
  new G4PVPlacement(
    0,                   
    G4ThreeVector(0, 0, -1 * cm),      
    basement_lvol,            
    "Basement",               
    logicWorld,                    
    false,                 
    0,                    
    checkOverlaps
  );       

  basement_lvol->SetRegion(AttenuatorRegion);
  AttenuatorRegion->AddRootLogicalVolume(basement_lvol);

  // construct ROOS chamber
  // corp
  G4Tubs* roos_corp_svol = new G4Tubs("Roos", 0, 22 * mm, 5 * mm, 0, CLHEP::twopi);
  G4LogicalVolume* roos_corp_lvol = new G4LogicalVolume(roos_corp_svol, PMMA, "Roos");
  new G4PVPlacement(
    0,
    G4ThreeVector(0, 0, (0.5 * cm - 1 * um)),
    roos_corp_lvol,
    "Roos",
    basement_lvol,
    false,
    0,
    checkOverlaps
  );

  G4UserLimits* ulimRoos = new G4UserLimits(0.05 * CLHEP::mm);
  roos_corp_lvol->SetUserLimits(ulimRoos);
  roos_corp_lvol->SetRegion(AttenuatorRegion);
  AttenuatorRegion->AddRootLogicalVolume(roos_corp_lvol);

  // cavity
  G4Tubs* cavity_svol = new G4Tubs("Cavity", 0, 8*mm, 1*mm,0,CLHEP::twopi);
  G4LogicalVolume* cavity_lvol = new G4LogicalVolume(cavity_svol, air, "Cavity");
  new G4PVPlacement(
    0,
    G4ThreeVector(0, 0, 2*mm),
    cavity_lvol,
    "Cavity",
    roos_corp_lvol,
    false,
    0,
    checkOverlaps
  );

  G4UserLimits* ulimCavity = new G4UserLimits(0.001 * CLHEP::mm);
  cavity_lvol->SetUserLimits(ulimCavity);
  cavity_lvol->SetRegion(IonChamberRegion);
  IonChamberRegion->AddRootLogicalVolume(cavity_lvol);

  // electrode
  G4double electrode_thickness = 0.5 * mm;
  G4Tubs* electrode_svol = new G4Tubs("Electrode", 0, 8*mm - 1* mm, electrode_thickness / 2.0, 0, CLHEP::twopi);
  G4LogicalVolume* electrode_lvol = new G4LogicalVolume(electrode_svol, Cu, "Electrode");
  new G4PVPlacement(
    0,
    G4ThreeVector(0, 0, 1*mm - electrode_thickness / 2.0),
    electrode_lvol,
    "Electrode",
    roos_corp_lvol,
    false,
    0,
    checkOverlaps
  );

  // TODO Прописать в TrackingAction или EventAction логику отслеживания электронов, остановившихся в Electrode
  // G4UserLimits* ulimElectrode = new G4UserLimits(0.1 * CLHEP::mm);
  // electrode_lvol->SetUserLimits(ulimElectrode);
  // TODO replace '0.05 * CLHEP::mm' with hyperparameter
  // G4UserLimits* ulimElectrode = new G4UserLimits(0.05 * CLHEP::mm);
  // electrode_lvol->SetUserLimits(ulimElectrode);
  // electrode_lvol->SetRegion(IonChamberRegion);
  // IonChamberRegion->AddRootLogicalVolume(electrode_lvol);

  // TODO поместить электрод

  // construct attenuator
  if(this->thickness > 0) {
    G4Box* attenuator_svol = new G4Box("Attenuator", 15*cm,15*cm,this->thickness / 2.0);
    G4LogicalVolume* attenuator_lvol = new G4LogicalVolume(attenuator_svol, this->material, "Attenuator");
    new G4PVPlacement(
      0,
      *(this->attenuator_tlate),
      attenuator_lvol,
      "Attenuator",
      logicWorld,
      false,
      0,
      checkOverlaps
    );

    // G4UserLimits* ulimAttenuator = new G4UserLimits(0.097 * CLHEP::mm);
    // attenuator_lvol->SetUserLimits(ulimAttenuator);
    G4UserLimits* ulimAttenuator = new G4UserLimits(0.05 * CLHEP::mm);
    attenuator_lvol->SetUserLimits(ulimAttenuator);
    attenuator_lvol->SetRegion(AttenuatorRegion);
    AttenuatorRegion->AddRootLogicalVolume(attenuator_lvol);
  }

  return physWorld;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

}
