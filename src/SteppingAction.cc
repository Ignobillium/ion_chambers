//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file SteppingAction.cc
/// \brief Implementation of the B1::SteppingAction class

#include "SteppingAction.hh"
#include "EventAction.hh"
#include "DetectorConstruction.hh"

#include "G4Step.hh"
#include "G4Event.hh"
#include "G4RunManager.hh"
#include "G4LogicalVolume.hh"
#include "G4AnalysisManager.hh"

namespace B1
{

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SteppingAction::SteppingAction(EventAction* eventAction)
: fEventAction(eventAction)
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SteppingAction::~SteppingAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SteppingAction::UserSteppingAction(const G4Step* step)
{
  auto track = step->GetTrack();
  auto vname = track->GetTouchableHandle()->GetVolume()->GetName();

  if ((vname == "Cavity") && (track->GetTrackID() > 1)) {

    // G4cout << "SSSS " << track->GetTrackID() << ' ' << pname << G4endl;
    //
    auto analysis = G4AnalysisManager::Instance();
    analysis->FillH1(0, 0, step->GetTotalEnergyDeposit());
    // G4cout << "Cavity" << '\t' << step->GetTotalEnergyDeposit() << G4endl;
    // TODO почему строка ниже не работает как надо?
    // fEventAction->AddCavityDose(step->GetTotalEnergyDeposit());
    // TODO уйти от гистограмм, сделать Accumulable
  } 
  else if (vname == "Electrode" && step->IsLastStepInVolume()) {
    // G4cout << "Electrode" << G4endl;
    // G4AnalysisManager::Instance()->FillH1(0, 1, 1);
    // G4cout << "Частица влетела в электрод" << G4endl;
    auto pname = track->GetParticleDefinition()->GetParticleName();
    if (pname == "e-") {
      // fEventAction->AddElectrodeCharge(+1);
      G4AnalysisManager::Instance()->FillH1(0, 1, 1);
    } else if (pname == "e+") {
      // fEventAction->AddElectrodeCharge(-1);
      G4AnalysisManager::Instance()->FillH1(0, 1, -1);
    }
  } 
  else if (vname == "Attenuator") {
    // G4cout << "Attenuator" << G4endl;
    // auto pos = track->GetPosition().z() / CLHEP::mm;
    auto post = step->GetPostStepPoint()->GetPosition();
    auto pres = step->GetPreStepPoint()->GetPosition();
    G4ThreeVector pos = pres + (post - pres) * G4UniformRand();
    auto analysis = G4AnalysisManager::Instance();
    analysis->FillH1(1, pos.z(), step->GetTotalEnergyDeposit());
  }

  // TODO Если заряженная частица вылетела из электрода, уменьшаем поглощеный заряд
  // if (step->GetPreStepPoint()->GetTouchableHandle()->GetVolume()->GetName() == "Electrode") {
  //   G4cout << "Частица покинула электрод" << G4endl;
  //   auto pname = track->GetParticleDefinition()->GetParticleName();
  //   if (pname == "e-") {
  //     fEventAction->AddElectrodeCharge(+1);
  //   } else if (pname == "e+") {
  //     fEventAction->AddElectrodeCharge(-1);
  //   }
  // }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

}
