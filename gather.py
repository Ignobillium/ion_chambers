#!/bin/python3
import os
for node in range(2, 12):
    print("gather from node", node)
    # zip files at node && scp tar
    cmd = "ssh hws@ws%d 'cd %s; tar -cf %s.tar.gz build/task*; scp %s.tar.gz ws1:%s'" % (node, os.getcwd(), node, node, os.getcwd())
    print(cmd)
    os.system(cmd)
    # unzip there
    os.system("tar -xf %s.tar.gz" % node)
    os.system("rm %s.tar.gz" % node)