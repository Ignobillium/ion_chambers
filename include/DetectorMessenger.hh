//
/// \file KhankinDetectorMessenger.hh
/// \brief Definition of the Khankin::DetectorMessenger class

#ifndef DetectorMessenger_hh
#define DetectorMessenger_hh 1

#include "globals.hh"
#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithADouble;
class G4UIcmdWithAnInteger;

namespace B1
{

class DetectorConstruction;

/// Messenger class that defines commands for DetectorConstruction.
///
/// It implements commands:
/// - //det/setPlateMaterial G4_NotationMaterial
/// - //det/setPlateThickness value unit

class DetectorMessenger: public G4UImessenger
{
  public:
    DetectorMessenger(DetectorConstruction* );
    ~DetectorMessenger() override;

    void SetNewValue(G4UIcommand*, G4String) override;

  private:
    DetectorConstruction*  fDetectorConstruction = nullptr;

    G4UIdirectory*         fDirectory = nullptr;
    G4UIdirectory*         fDetDirectory = nullptr;
    G4UIdirectory*         fDetGetDirectory = nullptr;
    G4UIdirectory*         fDetSetDirectory = nullptr;


    G4UIcmdWithAString*           fSetPlateMaterial = nullptr;
    G4UIcmdWithADoubleAndUnit*    fSetPlateThickness = nullptr;
};

}

#endif
