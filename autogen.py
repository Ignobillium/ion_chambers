#!/bin/python3
import os
import json
import pytest
import numpy as np
from numpyencoder import NumpyEncoder

def t_max(E, rho):
    """
    Возвращает максимальный пробег электронов в среде (мм)
    E (keV0
    rho (g / cm3))
    """
    # array([0.00258009, 1.07749774])
    return 0.00258009*np.power(E, 1.07749774) / rho

def t100(E, rho):
    """
    Возвращает положение пика максимума дозы (мм)
    E (keV0
    rho (g / cm3))
    """
    # array([0.00135099, 1.10950878])
    return 0.00135099*np.power(E, 1.10950878) / rho

def lopt(E, rho):
    """
    Возвращает L_opt (мм)
    E (keV)
    rho (g / cm3))
    """
    # array([2.77508620e-04, 1.24349201e+00])
    return 2.77508620e-04*np.power(E, 1.24349201) / rho


if __name__ == '__main__':
    with open("config.json", "rb") as f:
        config = json.loads(f.read().decode())

    with open("run.template", "r") as f:
        template = f.read()

    Es = np.arange(config['emin'], config['emax'] + config['de'], config['de'])
    # Es = np.arange(config['emax'], config['emax'] + config['de'], config['de'])
    ts = np.concatenate([
        # np.linspace(0, t100(Es, 1), 5),
        np.linspace(0, lopt(Es, config['rho']), 8),
        np.linspace(lopt(Es, config['rho']), t_max(Es, config['rho']) + 3, 8)[1:]
    ]).T

    print(ts)
    
    res = []
    for j, e in enumerate(Es):
        for t in ts[j]:
            res.append({
                'E': e,
                't': t
            })


    np.random.shuffle(res)

    try:
        os.mkdir("mac")
    except:
        pass

    k = 0
    for i in range(len(res)):
        res[i]['id'] = k
        print(res[i])
        with open("mac/task%d.mac" % k, "w") as f:
            f.write(template.format(
                material=config['material'],
                thickness=res[i]["t"],
                energy=res[i]["E"],
                ssd=100+res[i]["t"]/10,
                printProgress=config['printProgress'],
                beamOn=config['beamOn'],
            ))
        k += 1

    with open("_gen.json", 'w') as file:
        json.dump(res, file, indent=4, sort_keys=True,
                separators=(', ', ': '), ensure_ascii=False,
                cls=NumpyEncoder)