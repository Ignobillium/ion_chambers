#!/bin/python3
import os
import sys

if __name__ == '__main__':
    if len(sys.argv) == 1:
        "cluster"
        ntasks = len(os.listdir("%s/mac" % os.getcwd())) #!hardcode
        nhosts = 11 #!hardcode
        dn = ntasks // nhosts
        for i in range(1, 12):
            start = (i-1)*dn
            end = (i)*dn - 1
            if i == 11:
                end = ntasks - 1
            print(i, '\t', start, '\t', end)
            #!hardcode
            os.system("ssh ws%d 'source /home/hws/g4install/share/Geant4-11.0.0/geant4make/geant4make.sh; cd ion_chambers; python3 cluster.py %d %d' &" % (
                i,
                start,
                end
            ))
            start = end+1
    else:
        os.chdir("build")
        os.system("make -j")
        "worker"
        start = int(sys.argv[1])
        end = int(sys.argv[2])
        for i in range(start, end+1):
            try:
                os.mkdir("task%d" % i)
            except:
                pass
            os.chdir("task%d" % i)
            os.system("cp ../../mac/task%d.mac ." % i)
            os.system("../exampleB1 ./task%d.mac" % i)
            os.chdir("..")

